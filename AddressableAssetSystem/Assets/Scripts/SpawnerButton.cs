﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnerButton : MonoBehaviour
{
    [SerializeField] private int _index;
    [SerializeField] private int _spawnCount = 1;


    public void RequestSpawnAsset()
    {
        Debug.Log("Request Spawn Asset");
        for (int i = 0; i < _spawnCount; i++)
        {
            FindObjectOfType<AssetSpawner>().Spawn(_index);
        }
    }

    private void OnValidate()
    {
        _index = transform.GetSiblingIndex();
        GetComponentInChildren<Text>().text = _index.ToString();
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
