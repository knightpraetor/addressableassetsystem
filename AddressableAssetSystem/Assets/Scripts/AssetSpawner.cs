﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class AssetSpawner : MonoBehaviour
{
    public List<AssetReference> listOfRef;

    private readonly Dictionary<AssetReference, List<GameObject>> _spawnedAssets = 
        new Dictionary<AssetReference, List<GameObject>>();


    private readonly Dictionary<AssetReference, Queue<Vector3>> _queuedSpawnRequests =
        new Dictionary<AssetReference, Queue<Vector3>>();

    private readonly Dictionary<AssetReference, AsyncOperationHandle<GameObject>> _asyncOperationHandles =
        new Dictionary<AssetReference, AsyncOperationHandle<GameObject>>();

    private void Awake()
    {
    }

    public void Spawn(int index)
    {
        Debug.Log($"Spawn {index}");
        Debug.Log($"List of Ref Count: {listOfRef.Count}");
        if (index < 0 || index >= listOfRef.Count)
            return;

        AssetReference assetReference = listOfRef[index];
        Debug.Log($"Asset Reference: {assetReference}");
        Debug.Log($"Asset Reference RunTime Key : {assetReference.RuntimeKey}");
        if (assetReference.RuntimeKeyIsValid() == false)
        {
            Debug.Log("Invalid Key " + assetReference.RuntimeKey.ToString());
        }

        if (_asyncOperationHandles.ContainsKey(assetReference))
        {
            Debug.Log("Operation Handle Contained in handles");
            if (_asyncOperationHandles[assetReference].IsDone)
                SpawnAssetFromLoadedReference(assetReference, GetRandomPosition());
            else
                EnqueueSpawnForAfterInitialization(assetReference);
            return;
        }

        LoadAndSpawn(assetReference);
    }

    private void LoadAndSpawn(AssetReference assetReference)
    {
        Debug.Log("LoadAndSpawn Called");
        var handle = Addressables.LoadAssetAsync<GameObject>(assetReference);
        _asyncOperationHandles.Add(assetReference, handle);
        //_asyncOperationHandles[assetReference] = handle;

        handle.Completed += (operation) =>
        {
            Debug.Log("Completed Event Fired");
            SpawnAssetFromLoadedReference(assetReference, GetRandomPosition());
            if (_queuedSpawnRequests.ContainsKey(assetReference))
            {
                while (_queuedSpawnRequests[assetReference]?.Any() == true)
                {
                    var position = _queuedSpawnRequests[assetReference].Dequeue();
                    SpawnAssetFromLoadedReference(assetReference, position);
                }
            }
        };
    }

    void EnqueueSpawnForAfterInitialization(AssetReference assetReference)
    {
        Debug.Log($"Enqueue Spawn After Initialization");
        if (_queuedSpawnRequests.ContainsKey(assetReference) == false)
            _queuedSpawnRequests[assetReference] = new Queue<Vector3>();

        _queuedSpawnRequests[assetReference].Enqueue(GetRandomPosition());
    }

    void SpawnAssetFromLoadedReference(AssetReference assetReference, Vector3 position)
    {
        Debug.Log($"Spawn Asset From Loaded Reference");
        assetReference.InstantiateAsync(position, Quaternion.identity).Completed += (asyncOperationHandle) =>
        {
            if (asyncOperationHandle.Result == null)
            {
                Debug.Log("Async Operation Returned Null.");
                return;
            }
            if (_spawnedAssets.ContainsKey(assetReference) == false)
            {
                _spawnedAssets[assetReference] = new List<GameObject>();
            }

            _spawnedAssets[assetReference].Add(asyncOperationHandle.Result);
            var notify = asyncOperationHandle.Result.AddComponent<NotifyOnDestroy>();
            notify.Destroyed += Remove;
            notify.AssetReference = assetReference;
        };
    }

    Vector3 GetRandomPosition()
    {
        return new Vector3(UnityEngine.Random.Range(-5, 5), 1, UnityEngine.Random.Range(-5, 5));
    }

    void Remove(AssetReference assetReference, NotifyOnDestroy obj)
    {
        Addressables.ReleaseInstance(obj.gameObject);

        _spawnedAssets[assetReference].Remove(obj.gameObject);
        if (_spawnedAssets[assetReference].Count == 0)
        {
            Debug.Log($"Removed all {assetReference.RuntimeKey}");

            if (_asyncOperationHandles[assetReference].IsValid())
                Addressables.Release(_asyncOperationHandles[assetReference]);

            _asyncOperationHandles.Remove(assetReference);
        }
    }

}
